# translation of katekonsoleplugin.po to Khmer
# Khoem Sokhem <khoemsokhem@khmeros.info>, 2008, 2009, 2010.
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-26 01:01+0000\n"
"PO-Revision-Date: 2010-12-14 10:10+0700\n"
"Last-Translator: Khoem Sokhem <khoemsokhem@khmeros.info>\n"
"Language-Team: Khmer <support@khmeros.info>\n"
"Language: km\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: KBabel 1.11.4\n"
"X-Language: km_KH\n"

#: kateconsole.cpp:54
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "អ្នក​មិន​មាន karma គ្រាប់​គ្រាន់​ដើម្បី​ចូល​ដំណើរការ​សែល ឬ​ការ​ត្រាប់តាម​ស្ថានីយ"

#: kateconsole.cpp:102 kateconsole.cpp:132 kateconsole.cpp:657
#, kde-format
msgid "Terminal"
msgstr "ស្ថានីយ"

#: kateconsole.cpp:141
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "បង្ហូរ​ទៅ​ស្ថានីយ"

#: kateconsole.cpp:145
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "ធ្វើ​សម​កាល​​កម្ម​ស្ថានីយ​ជាមួយ​នឹង​​ឯកសារ​បច្ចុប្បន្ន"

#: kateconsole.cpp:149
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr ""

#: kateconsole.cpp:154 kateconsole.cpp:505
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "ផ្ដោត​អារម្មណ៍​ស្ថានីយ"

#: kateconsole.cpp:160
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Focus Terminal"
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "ផ្ដោត​អារម្មណ៍​ស្ថានីយ"

#: kateconsole.cpp:374
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"តើ​អ្នក​ពិត​ជា​ចង់​បង្ហូរ​អត្ថបទ​ទៅ​កុងសូល​ឬ ? វា​​នឹង​ប្រតិបត្តិ​ពាក្យ​បញ្ជា​ណា​ដែល​មាន​សិទ្ធិ​អ្នកប្រើ​របស់​អ្នក ។"

#: kateconsole.cpp:375
#, kde-format
msgid "Pipe to Terminal?"
msgstr "បង្ហូរ​ទៅ​ស្ថានីយ ?"

#: kateconsole.cpp:376
#, kde-format
msgid "Pipe to Terminal"
msgstr "បង្ហូរ​ទៅស្ថានីយ"

#: kateconsole.cpp:404
#, fuzzy, kde-format
#| msgid "Sorry, can not cd into '%1'"
msgid "Sorry, cannot cd into '%1'"
msgstr "សូម​អភ័យទោស អ្នក​មិន​អាច​ស៊ីឌី​ទៅ​ក្នុង '%1'"

#: kateconsole.cpp:446
#, kde-format
msgid "Not a local file: '%1'"
msgstr ""

#: kateconsole.cpp:479
#, fuzzy, kde-format
#| msgid ""
#| "Do you really want to pipe the text to the console? This will execute any "
#| "contained commands with your user rights."
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"តើ​អ្នក​ពិត​ជា​ចង់​បង្ហូរ​អត្ថបទ​ទៅ​កុងសូល​ឬ ? វា​​នឹង​ប្រតិបត្តិ​ពាក្យ​បញ្ជា​ណា​ដែល​មាន​សិទ្ធិ​អ្នកប្រើ​របស់​អ្នក ។"

#: kateconsole.cpp:486
#, fuzzy, kde-format
#| msgid "Pipe to Terminal?"
msgid "Run in Terminal?"
msgstr "បង្ហូរ​ទៅ​ស្ថានីយ ?"

#: kateconsole.cpp:487
#, kde-format
msgid "Run"
msgstr ""

#: kateconsole.cpp:502
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "&Pipe to Terminal"
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "បង្ហូរ​ទៅ​ស្ថានីយ"

#: kateconsole.cpp:513
#, fuzzy, kde-format
#| msgid "Defocus Terminal"
msgid "Defocus Terminal Panel"
msgstr "មិន​ផ្ដោត​អារម្មណ៍​ស្ថានីយ"

#: kateconsole.cpp:514 kateconsole.cpp:515
#, fuzzy, kde-format
#| msgid "Focus Terminal"
msgid "Focus Terminal Panel"
msgstr "ផ្ដោត​​​ស្ថានីយ"

#: kateconsole.cpp:590
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr "ធ្វើ​សម​កាល​កម្ម​ស្ថានីយ​​ដោយ​ស្វ័យ​ប្រវត្តិ​​ជា​មួយ​នឹង​ឯកសារ​បច្ចុប្បន្ន​នៅ​ពេល​ដែល​អាច"

#: kateconsole.cpp:594 kateconsole.cpp:615
#, fuzzy, kde-format
#| msgid "Pipe to Terminal"
msgid "Run in terminal"
msgstr "បង្ហូរ​ទៅស្ថានីយ"

#: kateconsole.cpp:596
#, kde-format
msgid "&Remove extension"
msgstr ""

#: kateconsole.cpp:601
#, kde-format
msgid "Prefix:"
msgstr ""

#: kateconsole.cpp:609
#, kde-format
msgid "&Show warning next time"
msgstr ""

#: kateconsole.cpp:611
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""

#: kateconsole.cpp:622
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "កំណត់​​បរិស្ថាន​របស់​ &EDITOR អថេរ​ទៅកាន់​ 'kate -b'"

#: kateconsole.cpp:625
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr "សំខាន់​ ៖​ ឯកសារ​ត្រូវ​​បាន​បិទ​ ដើម្បី​បង្កើត​កម្មវិធី​កុង​សូល​បន្ត​​"

#: kateconsole.cpp:628
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr ""

#: kateconsole.cpp:631
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""

#: kateconsole.cpp:662
#, kde-format
msgid "Terminal Settings"
msgstr "ការ​កំណត់​ស្ថានីយ"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "ឧបករណ៍"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ខឹម សុខែម, ម៉ន ម៉េត, សេង សុត្ថា"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "khoemsokhem@khmeros.info,​​mornmet@khmeros.info,sutha@khmeros.info"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Kate Terminal"
#~ msgstr "ស្ថានីយ"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Terminal Panel"
#~ msgstr "ស្ថានីយ"

#~ msgid "Konsole"
#~ msgstr "Konsole"

#~ msgid "Embedded Konsole"
#~ msgstr "Konsole ដែល​បាន​បង្កប់"
